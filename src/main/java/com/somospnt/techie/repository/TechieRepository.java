package com.somospnt.techie.repository;

import com.somospnt.techie.domain.Techie;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

@RepositoryRestResource
public interface TechieRepository extends CrudRepository<Techie, Long> {
    
    List<Techie> findByFechaLessThan(@Param("fecha") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  LocalDate fecha);
    List<Techie> findByFechaGreaterThanEqual(@Param("fecha") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fecha);

}
