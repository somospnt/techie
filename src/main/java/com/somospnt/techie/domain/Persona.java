package com.somospnt.techie.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Persona implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String nombre;
    @OneToMany(mappedBy = "persona")
    List<Techie> techies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Techie> getTechies() {
        return techies;
    }

    public void setTechies(List<Techie> techies) {
        this.techies = techies;
    }

}
