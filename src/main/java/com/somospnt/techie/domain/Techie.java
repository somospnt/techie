package com.somospnt.techie.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Techie implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String titulo;
    private String descripcion;
    private LocalDate fecha;
    @ManyToOne
    @JoinColumn(name="id_persona")
    private Persona persona;
    private int horaMilitar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getHoraMilitar() {
        return horaMilitar;
    }

    public void setHoraMilitar(int horaMilitar) {
        this.horaMilitar = horaMilitar;
    }
    
}
