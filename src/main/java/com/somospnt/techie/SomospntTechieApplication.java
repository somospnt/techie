package com.somospnt.techie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SomospntTechieApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomospntTechieApplication.class, args);
	}
}
