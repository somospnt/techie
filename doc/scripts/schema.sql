CREATE TABLE persona (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL
);

CREATE TABLE techie (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(100) NOT NULL,
    descripcion VARCHAR(200) NOT NULL,
    fecha DATETIME,
    hora_militar INT,
    id_persona BIGINT NOT NULL,
    FOREIGN KEY (id_persona) REFERENCES persona(id)
);

INSERT INTO persona (nombre) 
VALUES('Laureano Clausi'),
('Carolina Barbiero'),
('Emiliano Tebes'),
('Leonardo De Seta'),
('Matías Zamorano'),
('Pablo Rivero'),
('William Alexis Herrera'),
('Juan Cernadas'),
('Eduardo Daniel Hernandez'),
('Francisco Andrés  Disalvo'),
('Martín Nicolás Bertucelli'),
('Manuel Sanchez Avalos'),
('Emiliano Ramieri'),
('Felipe Nahuel Gonzalez'),
('Jonathan Patricio Buzzetti Díaz')
('Brian Ezequiel MACIAS'),
('Marcos Andrés Díaz Olmos');


